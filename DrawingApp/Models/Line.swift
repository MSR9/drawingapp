//
//  Line.swift
//  DrawingApp
//
//  Created by EPITADMBP04 on 5/17/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import UIKit

struct Line {
    let strokeWidth : Float
    let color : UIColor
    var points : [CGPoint]
}
